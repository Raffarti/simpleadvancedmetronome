/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SLBACKEND_H
#define SLBACKEND_H

#include "backend.h"

class SLData;
class Engine;

class SLBackend : public Backend
{
    Q_OBJECT
    SLData *sldata;
    void loadSLBuffers();
public:
    SLBackend(Engine *engine);
    ~SLBackend();

    // Backend interface
public:
    void play() override;
    void stop() override;

    // QIODevice interface
public:
    bool open(OpenMode mode) override;
    void close() override;
};

#endif // SLBACKEND_H
