/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALBACKEND_H
#define ALBACKEND_H

#include "backend.h"

#include <QTimer>
#include <QObject>

class ALData;

class ALBackend : public Backend
{
    Q_OBJECT
public:
    ALBackend(Engine * engine);
    ~ALBackend();

private:
    QTimer m_timer;

    ALData *aldata;
    char * m_tmpData = nullptr;
    int m_tmpSize = 0;
    void loadALBuffers();
    void nextALBuffers();
    void unloadALBuffers();

    // Backend interface
public:
    void play() override;
    void stop() override;
private slots:
    void ALReset();

    // Backend interface

    // QIODevice interface
public:
    bool open(OpenMode mode) override;
    void close() override;
};

#endif // ALBACKEND_H
