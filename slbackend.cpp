/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "slbackend.h"

#include <SLES/OpenSLES.h>

#include "engine.h"
#include "sound.h"

constexpr int NUM_BUFFERS = 3;
class SLData {
public:
    char * buffers[NUM_BUFFERS];
    char ** buffer = buffers;
    SLBufferQueueItf bqPlayerBufferQueue;
    SLPlayItf bqPlayerPlay;
    SLObjectItf engineObject;
    SLEngineItf engineEngine;
    SLObjectItf outputMixObject;
    SLObjectItf bqPlayerObject;
    SLDataLocator_BufferQueue loc_bufq = {SL_DATALOCATOR_BUFFERQUEUE, NUM_BUFFERS};
    SLDataFormat_PCM format_pcm = {
        SL_DATAFORMAT_PCM,
        1,
        SL_SAMPLINGRATE_44_1,
        SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_SPEAKER_FRONT_CENTER,
        SL_BYTEORDER_LITTLEENDIAN
    };
    SLDataSource audioSrc = {&loc_bufq, &format_pcm};
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, nullptr};
    SLDataSink audioSnk = {&loc_outmix, nullptr};
    const SLInterfaceID ids = SL_IID_BUFFERQUEUE;
    const SLboolean req = SL_BOOLEAN_TRUE;

};

SLBackend::SLBackend(Engine * engine) : Backend("OpenSL ES", engine)
{
    sldata = new SLData();
}

SLBackend::~SLBackend()
{
    delete sldata;
}

void SLBackend::play()
{
    for (int i = 0; i < NUM_BUFFERS; i++) {
        loadSLBuffers();
    }
    (*sldata->bqPlayerPlay)->SetPlayState(sldata->bqPlayerPlay, SL_PLAYSTATE_PLAYING);
}

void SLBackend::stop()
{
    (*sldata->bqPlayerPlay)->SetPlayState(sldata->bqPlayerPlay, SL_PLAYSTATE_STOPPED);
    sldata->buffer = sldata->buffers;
}

bool SLBackend::open(QIODevice::OpenMode mode)
{

    slCreateEngine(&sldata->engineObject, 0, nullptr, 0, nullptr, nullptr);

    (*sldata->engineObject)->Realize(sldata->engineObject, SL_BOOLEAN_FALSE);
    (*sldata->engineObject)->GetInterface(sldata->engineObject, SL_IID_ENGINE, &sldata->engineEngine);
    (*sldata->engineEngine)->CreateOutputMix(sldata->engineEngine, &sldata->outputMixObject, 0, 0, 0);
    (*sldata->outputMixObject)->Realize(sldata->outputMixObject, SL_BOOLEAN_FALSE);

    setByteOrder(QSysInfo::LittleEndian);
    setSampleRate(44100);
    setSampleSize(16);
    setSampleType(SignedInt);

    sldata->loc_outmix.outputMix = sldata->outputMixObject;
    // create audio player
    (*sldata->engineEngine)->CreateAudioPlayer(sldata->engineEngine, &sldata->bqPlayerObject, &sldata->audioSrc, &sldata->audioSnk, 1, &sldata->ids, &sldata->req);
    (*sldata->bqPlayerObject)->Realize(sldata->bqPlayerObject, SL_BOOLEAN_FALSE);
    (*sldata->bqPlayerObject)->GetInterface(sldata->bqPlayerObject, SL_IID_PLAY, &sldata->bqPlayerPlay);
    (*sldata->bqPlayerObject)->GetInterface(sldata->bqPlayerObject, SL_IID_BUFFERQUEUE,
                                            &sldata->bqPlayerBufferQueue);

    (*sldata->bqPlayerBufferQueue)->RegisterCallback(sldata->bqPlayerBufferQueue, [](SLBufferQueueItf, void *backend){
        static_cast<SLBackend *>(backend)->loadSLBuffers();
    }, this);
    for (int i = 0; i < NUM_BUFFERS; i++) {
        sldata->buffers[i] = new char[bytesForDuration(2500)];
    }

    return Backend::open(mode);
}

void SLBackend::close()
{
    (*sldata->bqPlayerBufferQueue)->Clear(sldata->bqPlayerBufferQueue);
    for (int i = 0; i < NUM_BUFFERS; i++) {
        delete[] sldata->buffers[i];
    }
    (*sldata->bqPlayerObject)->Destroy(sldata->bqPlayerObject);
    (*sldata->outputMixObject)->Destroy(sldata->outputMixObject);
    (*sldata->engineObject)->Destroy(sldata->engineObject);
    Backend::close();
}

void SLBackend::loadSLBuffers()
{
    int size = bytesForDuration(2500);
    auto data = sldata->buffer;
    readData(*data, size);
    if (sldata->buffer - sldata->buffers == NUM_BUFFERS -1)
        sldata->buffer = sldata->buffers;
    else
        sldata->buffer++;
    if (sldata->bqPlayerBufferQueue)
        (*sldata->bqPlayerBufferQueue)->Enqueue(sldata->bqPlayerBufferQueue, *data, size);
}
