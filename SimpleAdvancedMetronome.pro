QT += quick quickcontrols2
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    backend.cpp \
        main.cpp \
    engine.cpp \
    sound.cpp

lupdate_only{
    SOURCES = *.qml
}

CONFIG+=qtquickcompiler

TRANSLATIONS = \
    translations/SimpleAdvancedMetronome_it_IT.ts

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

CONFIG += qmltypes
QML_IMPORT_NAME = raffarti.SimpleAdvancedMetronome
QML_IMPORT_MAJOR_VERSION = 1

HEADERS += \
    backend.h \
    engine.h \
    sound.h

android {
    QT += androidextras svg
    backends += SL
}

wasm {
    QT += svg
    backends += AL
    QMAKE_CXXFLAGS_RELEASE -= -O2
    QMAKE_CXXFLAGS_RELEASE *= -O3
    QMAKE_CFLAGS_RELEASE -= -O2
    QMAKE_CFLAGS_RELEASE *= -O3
    QMAKE_CXXFLAGS -= -O2
    QMAKE_CXXFLAGS += -s WASM_OBJECT_FILES=1 --closure 1 -s FILESYSTEM=0 -s STANDALONE_WASM=1
    QMAKE_CFLAGS -= -O2
    QMAKE_CFLAGS += -O3 -s WASM_OBJECT_FILES=1 --closure 1 -s FILESYSTEM=0 -s STANDALONE_WASM=1
    QMAKE_LFLAGS += -O3 -s WASM_OBJECT_FILES=1
}

qtHaveModule(multimedia) {
    backends *= QT
}

isEmpty(backends) {
    error("No backend is begin built. Please add at least one (`qmake backends="backend1 backend2 ..."` Available backends: QT, AL, SL, SDL2).")
}

contains(backends, QT) {
    SOURCES += qtbackend.cpp
    HEADERS += qtbackend.h
    QT += multimedia
    DEFINES += USE_QT=1
}

contains(backends, AL) {
    SOURCES += albackend.cpp
    HEADERS += albackend.h
    LIBS += -lopenal
    DEFINES += USE_AL=1
}

contains(backends, SDL2) {
    SOURCES += sdl2backend.cpp
    HEADERS += sdl2backend.h
    LIBS += -lSDL2
    DEFINES += USE_SDL2=1
    wasm {
        QMAKE_CXXFLAGS += -s USE_SDL=2
        QMAKE_CFLAGS += -s USE_SDL=2
    }
}

contains(backends, SL) {
    SOURCES += slbackend.cpp
    HEADERS += slbackend.h
    DEFINES += USE_SL=1
    LIBS += -lOpenSLES
}

DEFINES += DEFAULT_BACKEND='\\"$$first(backends)\\"'

DISTFILES += \
    About.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

isEmpty(GIT_DESCRIBE){
    GIT_DESCRIBE = $$system(git describe --tags --long)
}
isEmpty(GIT_DESCRIBE){
    message("WARNING: empty git describe")
} else {
    message("Git describe: $$GIT_DESCRIBE")
}

DEFINES += GIT_DESCRIBE=\\\"$$GIT_DESCRIBE\\\"

ANDROID_PACKAGE_SOURCE_DIR = \
    $$PWD/android

ANDROID_ABIS = armeabi-v7a arm64-v8a
