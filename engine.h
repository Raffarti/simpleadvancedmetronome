/***
 * Copyright 2019-2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QTime>
#include <QIODevice>
#include <QQueue>
#include <QUrl>
#include <QMap>
#include "backend.h"


#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
#include <QtQml>
#endif

class Sound;

class Engine : public QObject
{
    Q_OBJECT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    QML_ELEMENT
    QML_SINGLETON
#endif


public:
private:
    qreal m_bpm = 60;

    int m_time1 = 4;

    int m_time2 = 3;

    bool m_running = false;

    static Engine *m_instance;

    bool m_polyrythm = false;

    bool m_primary = true;

    bool m_secondary = false;

    typedef struct BackendInfo {
        QString name;
        Backend * (*constructor)(Engine*);
        Backend * operator ()(Engine *engine);
    } BackendInfo;

    static QMap<QString, BackendInfo> m_backends;

private:

    QVector<Sound *> m_sounds;
    Backend * m_backend;

public:
    Q_PROPERTY(qreal bpm READ bpm WRITE setBpm NOTIFY bpmChanged)
    Q_PROPERTY(int time1 READ time1 WRITE setTime1 NOTIFY time1Changed)
    Q_PROPERTY(int time2 READ time2 WRITE setTime2 NOTIFY time2Changed)
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(bool polyrythm READ polyrythm WRITE setPolyrythm NOTIFY polyrythmChanged)
    Q_PROPERTY(bool primary READ primary WRITE setPrimary NOTIFY primaryChanged)
    Q_PROPERTY(bool secondary READ secondary WRITE setSecondary NOTIFY secondaryChanged)
    Q_PROPERTY(Backend * backend READ backend NOTIFY backendChanged)
    Q_PROPERTY(QStringList backends READ backends CONSTANT)

    explicit Engine(QObject *parent = nullptr);
    ~Engine();

    qreal bpm() const;

    int time1() const;

    int time2() const;

    bool running() const;

    static Engine *getInstance();

    bool polyrythm() const;

    bool primary() const;

    bool secondary() const;

    QVector<Sound *> sounds() const;

    Q_INVOKABLE void openLink(QUrl url);

    Q_INVOKABLE bool isWASM();

    bool isRunning() const;

    Backend * backend() const;

    QStringList backends() const;

signals:
    void beatPlayed(int voice);

    void bpmChanged(qreal bpm);

    void time1Changed(int time1);

    void time2Changed(int time2);

    void runningChanged(bool running);

    void polyrythmChanged(bool polyrythm);

    void primaryChanged(bool primary);

    void secondaryChanged(bool secondary);

    void backendChanged(Backend * backend);

public slots:
    void setBpm(qreal bpm);

    void setTime1(int time1);

    void setTime2(int time2);

    void setRunning(bool running);

    void onRunningChanged(bool running);

    void setPolyrythm(bool polyrythm);

    void setPrimary(bool primary);

    void setSecondary(bool secondary);

    void setBackend(QString backend);

    friend class Backend;
};



#endif // ENGINE_H
