<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>About</name>
    <message>
        <location filename="../About.qml" line="44"/>
        <source>Version:</source>
        <translation>Versione:</translation>
    </message>
    <message>
        <location filename="../About.qml" line="58"/>
        <source>Author: %1</source>
        <translation>Autore: %1</translation>
    </message>
    <message>
        <location filename="../About.qml" line="64"/>
        <source>Project page: %1</source>
        <translation>Pagina del progetto: %1</translation>
    </message>
    <message>
        <location filename="../About.qml" line="70"/>
        <source>License: %1</source>
        <translation>Licenza: %1</translation>
    </message>
    <message>
        <location filename="../About.qml" line="77"/>
        <source>Donate</source>
        <translation>Dona</translation>
    </message>
    <message>
        <location filename="../About.qml" line="82"/>
        <source>Report Issue</source>
        <translation>Segnala problema</translation>
    </message>
    <message>
        <location filename="../About.qml" line="93"/>
        <source>Audio Backend:</source>
        <translation>Backend Audio:</translation>
    </message>
    <message>
        <location filename="../About.qml" line="95"/>
        <source>Format:</source>
        <translation>Formato:</translation>
    </message>
    <message>
        <location filename="../About.qml" line="97"/>
        <source>Sample Rate:</source>
        <translation>Frequenza di campionamento:</translation>
    </message>
    <message>
        <location filename="../About.qml" line="99"/>
        <source>Sample Type:</source>
        <translation>Tipo dei campioni:</translation>
    </message>
    <message>
        <location filename="../About.qml" line="100"/>
        <source>Unigned Integer</source>
        <translation>Intero senza segno</translation>
    </message>
    <message>
        <location filename="../About.qml" line="100"/>
        <source>Signed Integer</source>
        <translation>Intero con segno</translation>
    </message>
    <message>
        <location filename="../About.qml" line="100"/>
        <source>Floating Point</source>
        <translation>Virgola Mobile</translation>
    </message>
    <message>
        <location filename="../About.qml" line="101"/>
        <source>Sample Size:</source>
        <translation>Dimensione dei campioni:</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Metronome</source>
        <translation type="vanished">Metronomo</translation>
    </message>
    <message>
        <location filename="../main.qml" line="56"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../main.qml" line="75"/>
        <source>Beats per minute:</source>
        <translation>Battiti al minuto:</translation>
    </message>
    <message>
        <location filename="../main.qml" line="93"/>
        <source>Primary beat</source>
        <translation>Battito principale</translation>
    </message>
    <message>
        <location filename="../main.qml" line="99"/>
        <location filename="../main.qml" line="134"/>
        <source>Beats:</source>
        <translation>Battiti:</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">Silenzia</translation>
    </message>
    <message>
        <location filename="../main.qml" line="30"/>
        <source>SimpleAdvancedMetronome</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.qml" line="130"/>
        <source>Secondary beat</source>
        <translation>Battito secondario</translation>
    </message>
    <message>
        <location filename="../main.qml" line="153"/>
        <source>Polyrythm</source>
        <translation>Poliritmo</translation>
    </message>
    <message>
        <location filename="../main.qml" line="171"/>
        <source>Play</source>
        <translation>Avvia</translation>
    </message>
    <message>
        <location filename="../main.qml" line="177"/>
        <source>Tap-to-time</source>
        <translation>Batti tempo</translation>
    </message>
    <message>
        <source>License: %1</source>
        <translation type="vanished">Licenza: %1</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="vanished">Dona</translation>
    </message>
    <message>
        <source>Buffer status</source>
        <translation type="vanished">Stato del buffer</translation>
    </message>
    <message>
        <source>Buffer health</source>
        <translation type="vanished">Salute del buffer</translation>
    </message>
    <message>
        <location filename="../main.qml" line="194"/>
        <source>Show buffer health</source>
        <translation>Mostra salute del buffer</translation>
    </message>
    <message>
        <location filename="../main.qml" line="211"/>
        <source>Bad</source>
        <translation>Pessima</translation>
    </message>
    <message>
        <location filename="../main.qml" line="215"/>
        <source>Good</source>
        <translation>Buona</translation>
    </message>
    <message>
        <location filename="../main.qml" line="243"/>
        <source>Free open source software, no ads, no traking.</source>
        <translation>Software open source libero, niente pubblicità, non ti traccia.</translation>
    </message>
    <message>
        <source>Android App</source>
        <translation type="vanished">App per Android</translation>
    </message>
    <message>
        <location filename="../main.qml" line="260"/>
        <source>Contribute</source>
        <translation>Contribuisci</translation>
    </message>
    <message>
        <location filename="../main.qml" line="273"/>
        <source>Oops - Something went wrong</source>
        <translation>Ups - Qualcosa è andato storto</translation>
    </message>
    <message>
        <location filename="../main.qml" line="277"/>
        <source>The beats per minute were over 600. This is likely caused by something external to this application or unresonsive interface.</source>
        <translation>I battiti al minuto erano superiori a 600. Questo è verosimilmente causato da qualcosa di esterno a questa applicazione o da poca reattività dell&apos;interfaccia.</translation>
    </message>
    <message>
        <source>Author: %1</source>
        <translation type="vanished">Autore: %1</translation>
    </message>
    <message>
        <source>Project page: %1</source>
        <translation type="vanished">Pagina del progetto: %1</translation>
    </message>
    <message>
        <source>Doante</source>
        <translation type="vanished">Dona</translation>
    </message>
    <message>
        <source>Report Issue</source>
        <translation type="vanished">Segnala problema</translation>
    </message>
</context>
</TS>
