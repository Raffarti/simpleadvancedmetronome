/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qtbackend.h"

#include <QAudioOutput>

QtBackend::QtBackend(Engine *engine) : Backend("Qt", engine)
{
    m_format = QAudioDeviceInfo::defaultOutputDevice().preferredFormat();
    m_format.setCodec("audio/PCM");
    setByteOrder(m_format.byteOrder() == QAudioFormat::LittleEndian ? QSysInfo::LittleEndian : QSysInfo::BigEndian);
    m_format.setChannelCount(1);
    m_format = QAudioDeviceInfo::defaultOutputDevice().nearestFormat(m_format);
    setSampleSize(m_format.sampleSize());
    setSampleRate(m_format.sampleRate());
    switch (m_format.sampleType()) {
    case QAudioFormat::SignedInt:
        setSampleType(SignedInt);
        break;
    case QAudioFormat::UnSignedInt:
        setSampleType(UnSignedInt);
        break;
    case QAudioFormat::Float:
        setSampleType(Float);
        break;
    default:
        qWarning() << "No valid format for QtBackend";
    }

    m_out = new QAudioOutput(m_format);
}

void QtBackend::play()
{
    m_out->reset();
    m_out->start(this);
}

void QtBackend::stop()
{

    m_out->stop();
}

bool QtBackend::open(QIODevice::OpenMode mode)
{
    return Backend::open(mode);
}

void QtBackend::close()
{
    Backend::close();
}
