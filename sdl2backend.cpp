/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdl2backend.h"
#include <SDL2/SDL_audio.h>

SDL2Backend::SDL2Backend(Engine *engine) : Backend("SDL2", engine)
{

}

void sdl_wrapper(void *userdata, Uint8 *stream, int len){
    static_cast<SDL2Backend *>(userdata)->read(reinterpret_cast<char*>(stream), len);
}

void SDL2Backend::play()
{
    SDL_PauseAudio(0);

}

void SDL2Backend::stop()
{
    SDL_PauseAudio(1);
    SDL_ClearQueuedAudio(1);
}

bool SDL2Backend::open(QIODevice::OpenMode mode)
{
    static SDL_AudioSpec spec;
    spec.format = AUDIO_S16SYS;
    spec.channels = 1;
    spec.userdata = this;
    spec.callback = sdl_wrapper;
    spec.freq = 44100;
    spec.samples = 1024;

    if (SDL_OpenAudio(&spec, &spec) < 0)
        qWarning() << SDL_GetError();
    setSampleRate(spec.freq);
    setByteOrder(spec.format & 0x1000 ? QSysInfo::BigEndian : QSysInfo::LittleEndian);
    setSampleSize(spec.format & 0xFF);
    setSampleType(spec.format & 0x100 ? Float : (spec.format & 0x8000 ? SignedInt : UnSignedInt));
    return Backend::open(mode);
}

void SDL2Backend::close()
{
    SDL_CloseAudio();
    Backend::close();
}
