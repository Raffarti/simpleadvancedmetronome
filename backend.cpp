/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "backend.h"
#include "engine.h"
#include "sound.h"

#include <QDataStream>
#include <QtMath>
#include <QDebug>

Backend::Backend(QString name, Engine * engine, bool hasProgress) : QIODevice(engine), m_name(name), m_hasProgress(hasProgress), m_engine(engine)
{

}


qint64 Backend::readData(char *data, qint64 maxlen)
{
    qreal rate = sampleRate();
    int bytes = sampleSize()/8;
    QByteArray a;
    QDataStream out(&a, QIODevice::ReadWrite);
    out.setByteOrder(byteOrder() == ByteOrder::LittleEndian ? QDataStream::LittleEndian : QDataStream::BigEndian);
    int i;
    for (i = 0; i < maxlen/bytes; i++) {
        auto b = m_prog/rate/(60.l/m_engine->m_bpm);
        auto bb = qFloor(b);
        auto sb = m_prog/rate/(60./m_engine->m_bpm * ( m_engine->m_polyrythm ? m_engine->m_time1 : 1 ) / m_engine->m_time2);
        auto sbb = qFloor(sb);
        if (m_last_bb != bb && m_engine->m_primary) {
            m_activeSounds.enqueue({m_sounds[bb % m_engine->m_time1 == 0?0:1], bb*rate*(60./m_engine->m_bpm)});
            m_last_bb = bb;
        }
        if (m_last_sbb != sbb && m_engine->m_secondary) {
            m_activeSounds.enqueue({m_sounds[2],sbb*rate*(60./m_engine->m_bpm * ( m_engine->m_polyrythm ? m_engine->m_time1 : 1 ) / m_engine->m_time2)});
            m_last_sbb = sbb;
        }
        if (!data) {
            m_prog++;
            continue ;
        }

        qreal s = 0;

        auto bbbi = QMutableListIterator<QPair<SoundInstance *, qreal>>(m_activeSounds);
        while ( bbbi.hasNext() ) {
            auto bbb = bbbi.next();
            int x = (m_prog - bbb.second);
            if (x >= bbb.first->size()) {
                bbbi.remove();
                continue;
            }
            s += bbb.first->get(x)*m_volume;
        }
        if (std::abs(s) > 1) {
            m_volume /= std::abs(s);
            s = s * m_volume;
         //   qDebug() << m_volume;
        } else m_volume = m_volume * (1 - 1 / rate / 100) + 1 / (rate * 100);
        for (int c = 0; c < 1 /*channelCount*/; c++){
            if (sampleType() == SampleType::SignedInt)
                switch (sampleSize()) {
                case 32:
                    out << static_cast<qint32>(s * std::numeric_limits<qint32>::max());
                    break;
                case 16:
                    out << static_cast<qint16>(s * std::numeric_limits<qint16>::max());
                    break;
                case 8:
                    out << static_cast<qint8>(s * std::numeric_limits<qint8>::max());
                    break;
                default:
                    qDebug() << "Unsupported signed size:" << sampleSize();
                }
            else if (sampleType() == SampleType::UnSignedInt) {
                s = (s + 1)/2;
                switch (sampleSize()) {
                case 32:
                    out << static_cast<quint32>(s * std::numeric_limits<quint32>::max());
                    break;
                case 16:
                    out << static_cast<quint16>(s * std::numeric_limits<quint16>::max());
                    break;
                case 8:
                    out << static_cast<quint8>(s * std::numeric_limits<quint8>::max());
                    break;
                default:
                    qDebug() << "Unsupported unsigned size:" << sampleSize();
                }
            } else if (sampleType() == SampleType::Float) {
                switch (sampleSize()) {
                case 32:
                    out.setFloatingPointPrecision(QDataStream::SinglePrecision);
                    out << s;
                    break;
                default:
                    qDebug() << "Unsupported unsigned size:" << sampleSize();
                }
            }
        }
        m_prog++;
    }
    out.device()->reset();
    int written = i*bytes*1/*channelCount*/;
    out.readRawData(data, written);
    return written;
}

qint64 Backend::writeData(const char *, qint64)
{
    Q_UNREACHABLE();
}

qint32 Backend::bytesForDuration(qint64 usec) const
{
    return qCeil(usec*sampleRate()/1'000'000.)*(sampleSize()/8);
}

qreal Backend::volume() const
{
    return m_volume;
}

void Backend::setVolume(const qreal &volume)
{
    m_volume = volume;
}

int Backend::sampleRate() const
{
    return m_sampleRate;
}

Backend::SampleType Backend::sampleType() const
{
    return m_sampleType;
}

int Backend::sampleSize() const
{
    return m_sampleSize;
}

Backend::ByteOrder Backend::byteOrder() const
{
    return m_byteOrder;
}

qreal Backend::buffer() const
{
    return m_buffer;
}

bool Backend::open(QIODevice::OpenMode mode)
{
    auto ss = m_engine->sounds();
    m_sounds.resize(ss.size());
    std::transform(ss.begin(), ss.end(), m_sounds.begin(), [this](Sound *a){return a->make(sampleRate()); });
    return QIODevice::open(mode);
}

void Backend::close()
{
    foreach (auto s, m_sounds)
        delete s;
    m_sounds.clear();
}

bool Backend::reset(){
    m_last_bb = -1;
    m_activeSounds.clear();
    m_prog = 0;
    return true;
}

void Backend::setBuffer(qreal buffer)
{
    if (qFuzzyCompare(m_buffer, buffer))
        return;

    m_buffer = buffer;
    emit bufferChanged(m_buffer);
}

void Backend::setSampleRate(int rate)
{
    if (rate == m_sampleRate) return;

    m_sampleRate = rate;
    emit sampleRateChanged(rate);
}

void Backend::setSampleType(Backend::SampleType type)
{
    if (type == m_sampleType) return;

    m_sampleType = type;
    emit sampleTypeChanged(type);
}

void Backend::setSampleSize(int size)
{
    if (size == m_sampleSize) return;

    m_sampleSize = size;
    emit sampleSizeChanged(size);
}

void Backend::setByteOrder(Backend::ByteOrder order)
{
    if (order == m_byteOrder) return;

    m_byteOrder = order;
    emit byteOrderChanged(order);
}
