/***
 * Copyright 2019-2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QQmlContext>
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
#include "engine.h"
#include "backend.h"
#endif

int main(int argc, char *argv[])
{
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    app.setOrganizationName("raffarti");
    app.setApplicationName("SimpleAdvancedMetronome");
    app.setApplicationDisplayName("SAM - Simple Advanced Metronome");
    app.setApplicationVersion("1.0");
    QTranslator appTranslator;
    (void) appTranslator.load(":/translations/SimpleAdvancedMetronome_" + QLocale::system().name());
    app.installTranslator(&appTranslator);

    QQmlApplicationEngine engine;


#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    qmlRegisterSingletonType<Engine>(
                "raffarti.SimpleAdvancedMetronome",
                1, 0, "Engine", [](QQmlEngine *, QJSEngine *) -> QObject * {
                    return new Engine();
                });
    qmlRegisterUncreatableType<Backend>("raffarti.SimpleAdvancedMetronome", 1, 0, "Backend", "Uncreatable");
#endif

    engine.rootContext()->setContextProperty("gitVersion", GIT_DESCRIBE);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
