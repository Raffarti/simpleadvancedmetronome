# SimpleAdvancedMetronome (SAM)
 
A simple and accurate metronome. Tested on Linux/Android/WASM. Features accented first beat and a second beat for subdivision/polyrythms.

Written in C++ using Qt framework.

# Building SAM

## Requirements

Following Qt modules are required for compilation:
- QtBase
- QtMultimedia
- QtDeclarative
- QtQuickControls2
- QtTranslations
- QtTools (lrelease binary for translations)

### Extra platform build requirements

- Windows/Linux:
    - C++ compiler with c++11 standard support
- Android: [qt.io](https://doc.qt.io/qt-5/android.html)
    - QtSVG
    - QtAndroidExtras
    - Android NDK
    - Android SDK
    - Java
- WASM: [qt.io](https://wiki.qt.io/Qt_for_WebAssembly)
    - QtSVG
    - emscripten
    
## Preparation

### Android preparation

```bash
export PATH="/path/to/qt/root/qt_version/android_armv7/bin/:$PATH"
export ANDROID_NDK_ROOT=/path/to/ndk
export ANDROID_SDK_ROOT=/path/to/sdk

#JAVA_HOME defining might be necessary depending on your system
#export JAVA_HOME=/path/to/java
```

### WebAssembly preparation

```bash
export PATH="/path/to/qt/root/qt_version/wasm_32/bin/:$PATH"
source emsdk_env.sh
```

## Compilation

```bash
qmake
lrelease SimpleAdvancedMetronome.pro
make
```

### Android APK generation additional steps

```bash
make install INSTALL_ROOT=android-build
androiddeployqt --output android-build --input android-libSimpleAdvancedMetronome.so-deployment-settings.json --gradle
# see androiddeployqt --help for signature
# apk will be generated at android-build/build/outputs/apk/release/android-build-release-signed.apk
```
