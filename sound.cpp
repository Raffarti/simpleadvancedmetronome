/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "sound.h"

#include <QtMath>

SoundInstance::SoundInstance(Sound sound, int rate) : m_sound(sound), m_rate(rate), m_cacheSize(qCeil(sound.duration()*rate)), m_cache(new qreal[m_cacheSize])
{

}

SoundInstance::~SoundInstance()
{
    delete[] m_cache;
}

qreal SoundInstance::get(int i)
{
    if (m_cached != m_cacheSize) {
        qreal x = i/(long double)m_rate;
        m_cache[i] = qSin(x*M_PI*m_sound.freq())/qExp(x*M_PI*10)*m_sound.volume();
        if (m_lastCached == i -1 || m_lastCached == -1)
            m_cached++;
        else m_cached = 1;
        m_lastCached = i;
    }
    return m_cache[i];
}

int SoundInstance::size()
{
    return m_cacheSize;
}
Sound::Sound(int freq, qreal volume, qreal duration) : m_freq(freq), m_duration(duration), m_volume(volume)
{

}

qreal Sound::duration() const
{
    return m_duration;
}

qreal Sound::volume() const
{
    return m_volume;
}

qreal Sound::freq() const
{
    return m_freq;
}

SoundInstance * Sound::make(int rate) const
{
    return new SoundInstance(*this, rate);
}
