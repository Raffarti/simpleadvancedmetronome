/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOUND_H
#define SOUND_H

#include <QtGlobal>

class SoundInstance;

class Sound {
    int m_freq;
    qreal m_duration;
    qreal m_volume;
public:
    Sound(int freq, qreal volume, qreal duration);
    inline qreal duration() const;
    inline qreal volume() const;
    inline qreal freq() const;
    SoundInstance * make(int rate) const;
};

class SoundInstance {
    Sound m_sound;
    int m_rate;
    int m_cached = 0;
    int m_cacheSize;
    int m_lastCached = -1;
    qreal * m_cache = nullptr;
private:
    SoundInstance(Sound sound, int rate);
public:
    ~SoundInstance();
    qreal get(int x);
    int size();
    friend class Sound;
};

#endif // SOUND_H
