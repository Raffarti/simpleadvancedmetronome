/***
 * Copyright 2019-2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtQuick.Window 2.15

import raffarti.SimpleAdvancedMetronome 1

ApplicationWindow {
    visible: true
    title: qsTr("SimpleAdvancedMetronome")
    width: Engine.isWASM() || visibility != Window.Windowed ? Screen.desktopAvailableWidth : Math.min(pane.implicitWidth, Screen.desktopAvailableWidth/2)
    height: Engine.isWASM() || visibility != Window.Windowed ? Screen.desktopAvailableHeight : Math.min(pane.implicitHeight, Screen.desktopAvailableHeight*2/3)
    id: window
    StackView {
        anchors.fill: parent
        id: stack
        initialItem: Page {
            id: pane
            contentItem: ScrollView {
                id: scroll
                clip: true
                padding: 8
                ColumnLayout {
                    id: cont
                    width: Math.max(scroll.availableWidth, implicitWidth)
                    Item {
                        Layout.fillWidth: true
                        height: 0
                        RoundButton {
                            anchors.right: parent.right
                            icon.source: "qrc:///icons/menu.svg"
                            onClicked: menu.visible = !menu.visible
                            Menu {
                                id: menu
                                MenuItem {
                                    text: qsTr("About")
                                    onClicked: stack.push(about)
                                }
                            }
                        }
                    }
                    Dial {
                        Layout.alignment: Qt.AlignHCenter
                        id: beatDeal
                        from: 20
                        to: 260
                        stepSize: 1
                        snapMode: Dial.SnapAlways
                        value: Engine.bpm
                        onMoved: Engine.bpm = value
                    }
                    RowLayout {
                        Layout.alignment: Qt.AlignHCenter
                        Label {
                            text: qsTr("Beats per minute:")
                        }
                        SpinBox {
                            id: beatField
                            property TextMetrics textMetrics: TextMetrics {
                                text: "999"
                                font: beatField.font
                            }
                            value: beatDeal.value
                            inputMethodHints: Qt.ImhFormattedNumbersOnly
                            from: beatDeal.from
                            to: beatDeal.to
                            Layout.preferredWidth: textMetrics.width + leftInset + rightInset + leftPadding + rightPadding
                            onValueModified: Engine.bpm = value
                            editable: true
                        }
                    }
                    GroupBox {
                        title: qsTr("Primary beat")
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: parent.implicitWidth
                        ColumnLayout {
                            RowLayout {
                                Label {
                                    text: qsTr("Beats:")
                                }
                                SpinBox {
                                    id: timeField1
                                    property TextMetrics textMetrics: TextMetrics {
                                        text: "99"
                                        font: timeField1.font
                                    }
                                    value: Engine.time1
                                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                                    from: 1
                                    stepSize: 1
                                    Layout.preferredWidth: textMetrics.width + leftInset + rightInset + leftPadding + rightPadding
                                    onValueModified: Engine.time1 = value
                                    editable: true
                                }
                            }
                            RowLayout {
                                Button {
                                    icon.source: "qrc:///icons/mute.svg"
                                    checkable: true
                                    onToggled: Engine.primary = !checked
                                    checked: !Engine.primary
                                }
                            }
                        }
                    }

                    GroupBox {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: parent.implicitWidth
                        title: qsTr("Secondary beat")
                        ColumnLayout {
                            RowLayout {
                                Label {
                                    text: qsTr("Beats:")
                                }
                                SpinBox {
                                    id: timeField2
                                    property TextMetrics textMetrics: TextMetrics {
                                        text: "99"
                                        font: timeField2.font
                                    }
                                    value: Engine.time2
                                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                                    from: 1
                                    stepSize: 1
                                    Layout.preferredWidth: textMetrics.width + leftInset + rightInset + leftPadding + rightPadding
                                    onValueModified: Engine.time2 = value
                                    editable: true
                                }
                            }
                            RowLayout {
                                Button {
                                    text: qsTr("Polyrythm")
                                    checkable: true
                                    onToggled: Engine.polyrythm = checked
                                    checked: Engine.polyrythm
                                }

                                Button {
                                    icon.source: "qrc:///icons/mute.svg"
                                    checkable: true
                                    onToggled: Engine.secondary = !checked
                                    checked: !Engine.secondary
                                }
                            }
                        }
                    }
                    RowLayout {
                        Layout.alignment: Qt.AlignHCenter
                        Button {
                            text: qsTr("Play")
                            checkable: true
                            onCheckedChanged: Engine.running = checked
                        }
                        Button {
                            property double start
                            text: qsTr("Tap-to-time")
                            checkable: true
                            onCheckedChanged: {
                                if (checked)
                                    start = new Date().getTime();
                                else {
                                    var bpm = 60000/(new Date().getTime() - start)
                                    if (bpm > 600) {
                                        errorDialog.visible = true
                                    } else Engine.bpm = bpm
                                }
                            }
                        }
                    }
                    ToolButton {
                        Layout.preferredWidth: parent.implicitWidth
                        Layout.alignment: Qt.AlignHCenter
                        text: qsTr("Show buffer health")
                        visible: Engine.backend.hasProgress
                        checkable: true
                        id: bufferBtn
                    }

                    ProgressBar {
                        Layout.preferredWidth: parent.implicitWidth
                        Layout.alignment: Qt.AlignHCenter
                        value: Engine.backend.buffer
                        visible: Engine.backend.hasProgress && bufferBtn.checked
                    }
                    RowLayout {
                        Layout.preferredWidth: parent.implicitWidth
                        Layout.alignment: Qt.AlignHCenter
                        visible: Engine.backend.hasProgress && bufferBtn.checked
                        Label {
                            text: qsTr("Bad")
                        }
                        Label {
                            Layout.alignment: Qt.AlignRight
                            text: qsTr("Good")
                        }
                    }

                }
            }
            footer: Frame {
                topPadding: 0
                bottomPadding: 0
                height: footerContents.visible ? implicitHeight : footerToggleButton.height
                Page {
                    width: parent.width

                    background: Item{}
                    header: ToolButton {
                        id: footerToggleButton
                        icon.source: footerContents.visible ? "qrc:///icons/arrow_small_down.svg" : "qrc:///icons/arrow_small_up.svg"
                        height: implicitHeight*0.66
                        onClicked: footerContents.visible = !footerContents.visible
                    }
                    Flow {
                        id: footerContents
                        spacing: 8
                        anchors.fill: parent
                        Label {
                            width: Math.min(parent.width, implicitWidth)
                            height: parent.width == width ? implicitHeight : andApp.height
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("Free open source software, no ads, no traking.")
                            wrapMode: Text.Wrap
                        }

                        RoundButton {
                            id: andApp
                            icon.source: "qrc:///icons/Android_logo_2019.svg"
                            onClicked: andPop.open()
                            Popup {
                                y: - height + parent.height
                                id: andPop
                                contentItem: ColumnLayout {
                                    ToolButton {
                                        text: "APK"
                                        onClicked: {
                                            Engine.openLink("https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/raw/SAM.apk?job=apk")
                                            andPop.close()
                                        }
                                    }
                                    ToolButton {
                                        text: "F-Droid"
                                        onClicked: {
                                            Engine.openLink("https://f-droid.org/packages/raffarti.simpleadvancedmetronome/")
                                            andPop.close()
                                        }
                                    }
                                }
                            }
                        }
                        RoundButton {
                            icon.source: "qrc:///icons/tux.png"
                            onClicked: Engine.openLink("https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/raw/SimpleAdvancedMetronome/SimpleAdvancedMetronome?job=Linux_x64")
                        }
                        RoundButton {
                            icon.source: "qrc:///icons/Windows_Logo.svg"
                            onClicked: Engine.openLink("https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/download?job=Win32")
                        }
                        Button {
                            text: qsTr("Contribute")
                            onClicked: stack.push(about)
                        }
                    }
                }
            }
        }
    }

    Dialog {
        x: (window.width - width)/2
        y: (window.height - height)/2
        id: errorDialog
        title: qsTr("Oops - Something went wrong")
        standardButtons: Dialog.Ok
        Label {
            anchors.fill: parent
            text: qsTr("The beats per minute were over 600. This is likely caused by something external to this application or unresonsive interface.")
            wrapMode: Text.Wrap
        }
    }
    Component {
        id: about
        About {
        }
    }
}
