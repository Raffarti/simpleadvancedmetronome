Un metronomo semplice ma accurato. Provato su Linux/Android/WASM. Disponibile anche per Windows. Il primo battito è accentato, la lunghezza della battuta è configurabile. Opzionalmente è possibile avere battiti addizionali per suddivisioni o poliritmi.

Scritto in C++ usando Qt. 
