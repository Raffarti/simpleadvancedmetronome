/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material 2.15
import QtQuick.Window 2.15

import raffarti.SimpleAdvancedMetronome 1

Page {
    header: ToolBar {
        ToolButton {
            icon.source: "qrc:///icons/arrow_back.svg"
            onClicked: stack.pop()
        }
    }
    contentItem: Pane {
        ColumnLayout {
            width: parent.width
            GridLayout {
                columnSpacing: 6
                Layout.fillWidth: true
                columns: versionLabel.implicitWidth + columnSpacing + versionArea.implicitWidth > parent.width ? 1 : 2
                Label {
                    id: versionLabel
                    text: qsTr("Version:")
                    Layout.alignment: Qt.AlignBaseline
                }
                TextArea {
                    id: versionArea
                    readOnly: true
                    text: gitVersion
                    selectByMouse: true
                    onFocusChanged: if (focus) selectAll()
                    Layout.alignment: Qt.AlignBaseline
                }
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Author: %1").arg("<a href='mailto:Raffarti <raffarti@zoho.com>'>Raffarti &lt;raffarti@zoho.com&gt;</a>")
                onLinkActivated: Engine.openLink(link)
                wrapMode: Text.Wrap
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Project page: %1").arg("<a href='https://gitlab.com/raffarti/simpleadvancedmetronome'>https://gitlab.com/raffarti/simpleadvancedmetronome</a>")
                onLinkActivated: Engine.openLink(link)
                wrapMode: Text.Wrap
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("License: %1").arg("<a href='https://www.gnu.org/licenses/gpl-3.0-standalone.html'>GPLv3</a>")
                onLinkActivated: Engine.openLink(link)
                wrapMode: Text.Wrap
            }
            Flow {
                Layout.fillWidth: true
                RoundButton {
                    text: qsTr("Donate")
                    onClicked: Engine.openLink("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GWAB237FHKB5S&source=url")
                    Material.background: Material.Yellow
                }
                RoundButton {
                    text: qsTr("Report Issue")
                    onClicked: Engine.openLink("https://gitlab.com/raffarti/simpleadvancedmetronome/issues")
                    Material.background: Material.Red
                }
            }
            Item {
                Layout.fillHeight: true
            }
            GridLayout {
                Layout.fillWidth: true
                columns: 2
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Audio Backend:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                ComboBox {
                    Layout.fillWidth: true
                    currentIndex: Engine.backends.indexOf(Engine.backend.name)
                    model: Engine.backends
                    onActivated: Engine.setBackend(currentText)
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Format:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: "PCM";
                    Layout.alignment: Qt.AlignRight
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Sample Rate:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: "%1KHz".arg(Engine.backend.sampleRate/1000)
                    Layout.alignment: Qt.AlignRight
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Sample Type:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    property var types: [qsTr("Unigned Integer"), qsTr("Signed Integer"), qsTr("Floating Point")]

                    Layout.fillWidth: true
                    text: types[Engine.backend.sampleType]
                    Layout.alignment: Qt.AlignRight
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Sample Size:")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
                Label {
                    Layout.fillWidth: true
                    text: "%1bit".arg(Engine.backend.sampleSize)
                    Layout.alignment: Qt.AlignRight
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }
            }
        }
    }
}
