# Intended for gitlab CI

SHELL := /bin/bash
QT5_DIR ?= /tmp/qt5
NDK ?= $(PWD)/android-ndk-r20
SDK ?= $(PWD)/android-sdk

.PHONY: pages

$(QT5_DIR):
	mkdir -p $(QT5_DIR)

install-qt.sh:
	curl -o install-qt.sh https://code.qt.io/cgit/qbs/qbs.git/plain/scripts/install-qt.sh
	chmod +x install-qt.sh
	sed -i.bak -e s/QT_VERSION/VERSION/ install-qt.sh

install-qt-wasm.sh: | install-qt.sh
	sed install-qt.sh -e 's|"qt5_$${VERSION//./}/qt.qt5.$${VERSION//./}.$${COMPONENT}.$${TOOLCHAIN}"|"qt5_$${VERSION//./}_wasm/qt.qt5.$${VERSION//./}.$${COMPONENT}.$${TOOLCHAIN}"|' -e 's|"qt5_$${VERSION//./}/qt.qt5.$${VERSION//./}.$${TOOLCHAIN}"|"qt5_$${VERSION//./}_wasm/qt.qt5.$${VERSION//./}.$${TOOLCHAIN}"|' > install-qt-wasm.sh
	chmod +x install-qt-wasm.sh
	
$(QT5_DIR)/5.15.2/wasm_32/bin/qmake: | install-qt-wasm.sh $(QT5_DIR)
	./install-qt-wasm.sh --version 5.15.2 --toolchain wasm_32 qtbase qttools qttranslations icu qtmultimedia qtsvg qtdeclarative qtquickcontrols2 --directory $(QT5_DIR)
	
emsdk:
	git clone https://github.com/emscripten-core/emsdk.git
	
emsdk/upstream: | emsdk
	emsdk/emsdk install 1.39.8
	emsdk/emsdk install fastcomp-clang-e1.38.30-64bit
	
~/.emscripten: | emsdk/upstream
	emsdk/emsdk activate 1.39.8
	
pages: | ~/.emscripten $(QT5_DIR)/5.15.2/wasm_32/bin/qmake
	source emsdk/emsdk_env.sh && $(QT5_DIR)/5.15.2/wasm_32/bin/lrelease SimpleAdvancedMetronome.pro && $(QT5_DIR)/5.15.2/wasm_32/bin/qmake backends="AL SDL2" && \
	make && patch < icon.patch &&\
	mv qtlogo.svg qtloader.js SimpleAdvancedMetronome.js SimpleAdvancedMetronome.html SimpleAdvancedMetronome.wasm Metronome.svg public && gzip -k public/SimpleAdvancedMetronome.wasm
	
$(QT5_DIR)/5.15.2/gcc_64/bin/lrelease: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --version 5.15.2 icu qttools qtbase --directory $(QT5_DIR)

$(QT5_DIR)/5.15.2/android/bin/qmake: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --version 5.15.2 --toolchain any --target android tools qtbase qttranslations qtandroidextras qtmultimedia qtsvg qtdeclarative qtquickcontrols2 --directory $(QT5_DIR)
	
$(NDK):
	curl -o android-ndk.zip https://dl.google.com/android/repository/android-ndk-r20-linux-x86_64.zip
	unzip  -q android-ndk.zip
	if  [[ ! -d "$(NDK)" ]]; then mv android-ndk "$(NDK)"; fi
	
$(SDK):
	curl -o android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
	unzip -q android-sdk.zip -d $(SDK)
	
$(SDK)/platforms/android-26: | $(SDK)
	$(SDK)/tools/bin/sdkmanager --update && \
	yes | $(SDK)/tools/bin/sdkmanager --licenses && \
	$(SDK)/tools/bin/sdkmanager "platforms;android-26" && \
	$(SDK)/tools/bin/sdkmanager "build-tools;28.0.2"
	
	
SAM.apk: | $(QT5_DIR)/5.15.2/android/bin/qmake $(NDK) $(SDK)/platforms/android-26
	export ANDROID_SDK_ROOT=$(SDK) ANDROID_NDK_ROOT="$(NDK)" && $(QT5_DIR)/5.15.2/android/bin/lrelease SimpleAdvancedMetronome.pro && $(QT5_DIR)/5.15.2/android/bin/qmake && make && \
	make install INSTALL_ROOT=android-build && $(QT5_DIR)/5.15.2/android/bin/androiddeployqt --output android-build --input android-SimpleAdvancedMetronome-deployment-settings.json --gradle $(ANDROIDDEPLOYQT_OPTS) && (mv android-build/build/outputs/apk/release/android-build-release-signed.apk SAM.apk || mv android-build/build/outputs/apk/release/android-build-release-unsigned.apk SAM.apk)

pwd:=$(shell pwd)
wineenv=$(WINEPREFIX) $(WINEENV) WINEDEBUG=-all
winepath=$(wineenv) winepath
wine=$(WINEPATH) $(wineenv) wine
WINEPREFIX=WINEPREFIX="$(pwd)/wine"
WINEPATH=WINEPATH='C:\Program Files (x86)\CMake\bin;$(shell $(winepath) -w "$(QT5_DIR)/Tools/mingw810_64/bin");$(shell $(winepath) -w "$(QT5_DIR)/5.15.2/mingw81_64/bin")' QTDIR="Z:$(QT5_DIR)/5.15.2/mingw81_64/"
qmake=$(wine) $(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe

.PHONY: build winbuild windeploy linuxdeploy
	
build:
	lrelease-qt5 SimpleAdvancedMetronome.pro
	mkdir -p build && cd build && qmake-qt5 .. -spec linux-g++ && make
	
linuxdeploy: build
	mkdir SimpleAdvancedMetronome
	cp build/SimpleAdvancedMetronome -f SimpleAdvancedMetronome/SimpleAdvancedMetronome
	
	
mingw.7z:
	wget -O mingw.7z http://download.qt.io/online/qtsdkrepository/windows_x86/desktop/tools_mingw/qt.tools.win64_mingw810/8.1.0-1-202004170606x86_64-8.1.0-release-posix-seh-rt_v6-rev0.7z
	
$(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe: | install-qt.sh $(QT5_DIR)
	./install-qt.sh --host windows_x86 --version 5.15.2 --toolchain win64_mingw81 --target desktop tools qtbase qttranslations qtmultimedia qtsvg qtdeclarative qtquickcontrols2 --directory $(QT5_DIR)
	
$(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf: $(QT5_DIR)/5.15.2/mingw81_64/bin/qmake.exe $(QT5_DIR)/Tools/mingw810_64
	echo "[paths]" > $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf
	echo "Prefix=../" >> $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf

$(QT5_DIR)/Tools/mingw810_64: mingw.7z
	7zr -o$(QT5_DIR)/ x mingw.7z
	touch -c $@

build_win32/SimpleAdvancedMetronome.exe: $(QT5_DIR)/5.15.2/mingw81_64/bin/qt.conf
	lrelease-qt5 SimpleAdvancedMetronome.pro
	mkdir -p build_win32 && cd build_win32 && $(qmake) $(qmake_opts) ../SimpleAdvancedMetronome.pro GIT_DESCRIBE="$(shell git describe --tags --long)"
	$(wine) mingw32-make -C build_win32

winbuild: build_win32/SimpleAdvancedMetronome.exe

build_win32/release/SimpleAdvancedMetronome.exe: winbuild

windeploy: build_win32/release/SimpleAdvancedMetronome.exe
	mkdir -p SimpleAdvancedMetronome/
	cp $(QT5_DIR)/Tools/mingw810_64/opt/bin/libeay32.dll -f SimpleAdvancedMetronome/libeay32.dll
	cp $(QT5_DIR)/Tools/mingw810_64/opt/bin/ssleay32.dll -f SimpleAdvancedMetronome/ssleay32.dll
	cp build_win32/release/SimpleAdvancedMetronome.exe -f SimpleAdvancedMetronome/
	$(wine) $(QT5_DIR)/5.15.2/mingw81_64/bin/windeployqt.exe --qmldir . SimpleAdvancedMetronome/SimpleAdvancedMetronome.exe
