/***
 * Copyright 2019-2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "engine.h"
#include <QtMath>
#include <QtEndian>

#include <QDataStream>
#include <QDebug>
#include <QStringList>

#include "sound.h"
#ifdef USE_AL
#include "albackend.h"
#endif
#ifdef USE_QT
#include "qtbackend.h"
#endif
#ifdef USE_SDL2
#include "sdl2backend.h"
#endif
#ifdef USE_SL
#include "slbackend.h"
#endif

#ifdef Q_OS_WASM
#include <emscripten.h>
#else
#include <QDesktopServices>
#endif

QMap<QString, Engine::BackendInfo> Engine::m_backends = {
#ifdef USE_QT
            {"QT", {"Qt", [](Engine *engine) -> Backend *{ return (Backend *) new QtBackend(engine);}}},
#endif
#ifdef USE_AL
            {"AL", {"OpenAL", [](Engine *engine) -> Backend *{ return (Backend *) new ALBackend(engine);}}},
#endif
#ifdef USE_SDL2
            {"SDL2", {"SDL2", [](Engine *engine) -> Backend *{ return (Backend *) new SDL2Backend(engine);}}},
#endif
#ifdef USE_SL
            {"SL", {"OpenSL ES", [](Engine *engine) -> Backend *{ return (Backend *) new SLBackend(engine);}}},
#endif
        };

Engine *Engine::m_instance = nullptr;


bool Engine::isRunning() const
{
    return m_running;
}

Backend *Engine::backend() const
{
    return m_backend;
}

QStringList Engine::backends() const
{
    QStringList r;
    foreach(auto s, m_backends) {
        r << s.name;
    }
    return r;
}

Engine::Engine(QObject *parent) : QObject(parent)
{
    connect(this, &Engine::runningChanged, this, &Engine::onRunningChanged);

    m_sounds.append(new Sound(4000, 1, 0.25));
    m_sounds.append(new Sound(2000, 1, 0.25));
    m_sounds.append(new Sound(6000, .5, 0.25));

    m_backend = m_backends[DEFAULT_BACKEND](this);

    m_instance = this;
    m_backend->open(QIODevice::ReadOnly|QIODevice::Unbuffered);
}

Engine::~Engine()
{
    setRunning(false);
}


qreal Engine::bpm() const
{
    return m_bpm;
}

int Engine::time1() const
{
    return m_time1;
}

int Engine::time2() const
{
    return m_time2;
}

bool Engine::running() const
{
    return m_running;
}

Engine *Engine::getInstance()
{
    if (!m_instance)
        m_instance = new Engine();
    return m_instance;
}

bool Engine::polyrythm() const
{
    return m_polyrythm;
}

bool Engine::primary() const
{
    return m_primary;
}

bool Engine::secondary() const
{
    return m_secondary;
}

QVector<Sound *> Engine::sounds() const
{
    return m_sounds;
}

void Engine::openLink(QUrl url)
{
#ifdef Q_OS_WASM
    EM_ASM({window.open(UTF8ToString($0), '_blank')}, url.toString().toUtf8().data());
#else
    QDesktopServices::openUrl(url);
#endif
}

bool Engine::isWASM()
{
#ifdef Q_OS_WASM
    return true;
#else
    return false;
#endif
}

void Engine::setBpm(qreal bpm)
{
    if (qFuzzyCompare(m_bpm, bpm))
        return;

    m_bpm = bpm;
    emit bpmChanged(m_bpm);
}

void Engine::setTime1(int time1)
{
    if (m_time1 == time1)
        return;

    m_time1 = time1;
    emit time1Changed(m_time1);
}

void Engine::setTime2(int time2)
{
    if (m_time2 == time2)
        return;

    m_time2 = time2;
    emit time2Changed(m_time2);
}

void Engine::setRunning(bool running)
{
    if (m_running == running)
        return;

    m_running = running;
    emit runningChanged(m_running);
}

void Engine::onRunningChanged(bool running)
{
    if (running) {
        m_backend->reset();
        m_backend->play();
    } else {
        m_backend->stop();
    }
}

void Engine::setPolyrythm(bool polyrythm)
{
    if (m_polyrythm == polyrythm)
        return;

    m_polyrythm = polyrythm;
    emit polyrythmChanged(m_polyrythm);
}

void Engine::setPrimary(bool primary)
{
    if (m_primary == primary)
        return;

    m_primary = primary;
    emit primaryChanged(m_primary);
}

void Engine::setSecondary(bool secondary)
{
    if (m_secondary == secondary)
        return;

    m_secondary = secondary;
    emit secondaryChanged(m_secondary);
}

void Engine::setBackend(QString backend)
{
    auto wasRunning = m_running;
    setRunning(false);
    m_backend->close();
    delete m_backend;
    foreach(auto b, m_backends) {
        if (b.name == backend) {
            m_backend = b(this);
            break;
        }
    }
    assert(m_backend);
    backendChanged(m_backend);
    m_backend->open(QIODevice::ReadOnly|QIODevice::Unbuffered);
    if (wasRunning)
        setRunning(true);
}



Backend *Engine::BackendInfo::operator ()(Engine *engine)
{
    return constructor(engine);
}
