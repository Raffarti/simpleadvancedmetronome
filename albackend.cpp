/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#include "albackend.h"

#include <AL/al.h>
#include <AL/alc.h>
#include <QTimer>
#include <QDebug>
#include "engine.h"
#ifdef Q_OS_WASM
#include <emscripten.h>
#define AL_FORMAT_MONO_FLOAT32                   0x10010
#define AL_FORMAT_STEREO_FLOAT32                 0x10011
#endif
constexpr unsigned int BUFFER_DURATION = 5'000;
constexpr unsigned int BATCH_SIZE = 10;
constexpr unsigned int NUM_BATCHES = 10;
constexpr unsigned int NUM_BUFFERS = BATCH_SIZE*NUM_BATCHES;

class ALData {
public:
    ALCcontext *context = nullptr;
    ALCdevice *device = nullptr;
    ALuint source;
    ALuint buffers[NUM_BUFFERS];
    ALuint *buffer = nullptr;
    ALenum format;
    int last_bb[NUM_BUFFERS];
    quint64 prog[NUM_BUFFERS];
    int total = 0;
};

ALBackend::ALBackend(Engine *engine) : Backend("OpenAL", engine, true)
{
    m_timer.setSingleShot(false);
    aldata = new ALData();
    aldata->buffer = aldata->buffers;
    setByteOrder(QSysInfo::LittleEndian);
    connect(&m_timer, &QTimer::timeout, this, &ALBackend::nextALBuffers);
    connect(m_engine, &Engine::bpmChanged, this, &ALBackend::ALReset);
    connect(m_engine, &Engine::primaryChanged, this, &ALBackend::ALReset);
    connect(m_engine, &Engine::secondaryChanged, this, &ALBackend::ALReset);
    connect(m_engine, &Engine::time1Changed, this, &ALBackend::ALReset);
    connect(m_engine, &Engine::time2Changed, this, &ALBackend::ALReset);
    connect(m_engine, &Engine::polyrythmChanged, this, &ALBackend::ALReset);
}

ALBackend::~ALBackend()
{
    delete aldata;
}

void ALBackend::play()
{
    m_tmpSize = bytesForDuration(BUFFER_DURATION);
    if (m_tmpData)
        delete[] m_tmpData;
    m_tmpData = new char[m_tmpSize];
    aldata->buffer = aldata->buffers;
    loadALBuffers();
    m_timer.setInterval(0.45 * BUFFER_DURATION * BATCH_SIZE / 1'000);
    m_timer.start();
}

void ALBackend::stop()
{
    m_timer.stop();
    alSourceStop(aldata->source);
    unloadALBuffers();
    alSourceRewind(aldata->source);
    aldata->total = 0;
    m_activeSounds.clear();
}

void ALBackend::loadALBuffers()
{
    for (unsigned int i = 0; i < BATCH_SIZE; i++) {
        if (aldata->total == NUM_BUFFERS)
            return;
        std::copy_backward(aldata->last_bb, aldata->last_bb + NUM_BUFFERS - 1, aldata->last_bb +NUM_BUFFERS);
        std::copy_backward(aldata->prog, aldata->prog + NUM_BUFFERS - 1, aldata->prog + NUM_BUFFERS);
        aldata->last_bb[0] = m_last_bb;
        aldata->prog[0] = m_prog;
        readData(m_tmpData, m_tmpSize);
        alBufferData(*aldata->buffer, aldata->format, m_tmpData, m_tmpSize, sampleRate());
        alGetError();
        alSourceQueueBuffers(aldata->source, 1, aldata->buffer);
        if (!alGetError())
            aldata->total++;

        if (++aldata->buffer == aldata->buffers + NUM_BUFFERS)
            aldata->buffer = aldata->buffers;
    }
}

void ALBackend::ALReset()
{
    if (!m_engine->isRunning())
        return;

    int processed;
    ALint state;
    alGetSourcei(aldata->source, AL_SOURCE_STATE, &state);
    alGetSourcei(aldata->source, AL_BUFFERS_PROCESSED, &processed);
    if (state == AL_INITIAL)
        alSourcePlay(aldata->source);
    alSourceStop(aldata->source);
    if (processed != NUM_BUFFERS) {
        m_last_bb = aldata->last_bb[NUM_BUFFERS - processed -1];
        m_prog = aldata->prog[NUM_BUFFERS - processed -1];
    }
    m_activeSounds.clear();
    read(nullptr, processed * m_tmpSize);
    unloadALBuffers();
    aldata->total = 0;
    aldata->buffer = aldata->buffers;
    alSourceRewind(aldata->source);
    loadALBuffers();
    m_timer.start();
}

bool ALBackend::open(QIODevice::OpenMode mode)
{

    aldata->device = alcOpenDevice(nullptr);
#ifdef Q_OS_WASM
        setSampleRate(EM_ASM_INT({
            var AudioContext = window.AudioContext || window.webkitAudioContext;
            var ctx = new AudioContext();
            var sr = ctx.sampleRate;
            ctx.close();
            return sr;
        }));

        if (alIsExtensionPresent("AL_EXT_float32")) {
            aldata->format = AL_FORMAT_MONO_FLOAT32;
            setSampleType(Float);
            setSampleSize(32);
        }
#else
    aldata->format = AL_FORMAT_MONO16;
    setSampleType(SignedInt);
    setSampleSize(16);
    ALint size;
    alcGetIntegerv(aldata->device, ALC_ATTRIBUTES_SIZE, 1, &size);
    ALint *data = new ALint[size];
    alcGetIntegerv(aldata->device, ALC_ALL_ATTRIBUTES, size, data);
    for (int i = 0; i < size * 2; i += 2){
        if (data[i*2] == ALC_FREQUENCY) {
            setSampleRate(data[i*2+1]);
            break;
        }
    }
    delete[] data;
#endif
    ALint attrlist[] = {ALC_FREQUENCY, sampleRate(), 0};
    aldata->context = alcCreateContext(aldata->device, attrlist);

    if (!aldata->context)
        return false;
    alcMakeContextCurrent(aldata->context);


    alGenSources(1, &aldata->source);

    alGenBuffers(NUM_BUFFERS, aldata->buffers);
    return Backend::open(mode);
}

void ALBackend::close()
{
    alDeleteSources(1, &aldata->source);
    alDeleteBuffers(NUM_BUFFERS, aldata->buffers);
    alcMakeContextCurrent(nullptr);
    alcDestroyContext(aldata->context);
    alcCloseDevice(aldata->device);
    Backend::close();
}

void ALBackend::nextALBuffers()
{
    unloadALBuffers();
    ALint state;
    alGetSourcei(aldata->source, AL_SOURCE_STATE, &state);
    if (state != AL_PLAYING)
        alSourcePlay(aldata->source);
    loadALBuffers();
    setBuffer((qreal)aldata->total / NUM_BUFFERS);
}

void ALBackend::unloadALBuffers()
{
    int processed;
    alGetSourcei(aldata->source, AL_BUFFERS_PROCESSED, &processed);

    if (processed) {
        auto head = aldata->buffer - aldata->total;
        if (head < aldata->buffers) {
            if (head + processed > aldata->buffers) {
                auto batch = aldata->total - (aldata->buffer - aldata->buffers);
                alGetError();
                alSourceUnqueueBuffers(aldata->source, batch, head + NUM_BUFFERS);
                if (!alGetError())
                    aldata->total -= batch;

                processed -= batch;
                head = aldata->buffers;
                if (!processed)
                    return;
            } else
                head += NUM_BUFFERS;
        }
        alGetError();
        alSourceUnqueueBuffers(aldata->source, processed, head);
        if (!alGetError())
            aldata->total -= processed;
    }

}
