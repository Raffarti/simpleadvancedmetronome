/***
 * Copyright 2020 Raffaele Pertile <raffarti@zoho.com>
 *
 * This file is part of raffarti.SimpleAdvancedMetronome.
 *
 * raffarti.SimpleAdvancedMetronome is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * raffarti.SimpleAdvancedMetronome is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with raffarti.SimpleAdvancedMetronome. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BACKEND_H
#define BACKEND_H

#include <QIODevice>
#include <QQueue>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
#include <QtQml>
#endif

class SoundInstance;

class Engine;

class Backend : public QIODevice
{
    Q_OBJECT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    QML_ELEMENT
    QML_UNCREATABLE("")
#endif

public:
    enum SampleType {
        UnSignedInt,
        SignedInt,
        Float
    };
    Q_ENUM(SampleType);

    using ByteOrder = QSysInfo::Endian;
    Q_ENUM(ByteOrder);

    Q_PROPERTY(int sampleRate READ sampleRate NOTIFY sampleRateChanged)
    Q_PROPERTY(int sampleSize READ sampleSize NOTIFY sampleSizeChanged)
    Q_PROPERTY(SampleType sampleType READ sampleType NOTIFY sampleTypeChanged)
    Q_PROPERTY(ByteOrder byteOrder READ byteOrder NOTIFY byteOrderChanged)
    Q_PROPERTY(qreal buffer READ buffer NOTIFY bufferChanged)
    Q_PROPERTY(bool hasProgress MEMBER m_hasProgress CONSTANT)
    Q_PROPERTY(QString name MEMBER m_name CONSTANT)

private:
    int m_sampleRate = -1;
    int m_sampleSize = -1;
    SampleType m_sampleType = SignedInt;
    ByteOrder m_byteOrder = QSysInfo::LittleEndian;
    const QString m_name;
    const bool m_hasProgress;
protected:
    Engine * m_engine = nullptr;

    int m_last_bb = -1;
    int m_last_sbb = -1;
    qint64 m_lastPrimary = 0;
    qint64 m_lastSecondary = 0;
    qreal m_volume = 1;
    QVector<SoundInstance *> m_sounds;
    quint64 m_prog = 0;
    qreal m_buffer = 0;
    QQueue<QPair<SoundInstance *, double>> m_activeSounds;

    qint64 writeData(const char *, qint64) final override;
    qint64 readData(char *, qint64) final override;

    void setBuffer(qreal buffer);

    void setSampleRate(int rate);
    void setSampleType(SampleType type);
    void setSampleSize(int size);
    void setByteOrder(ByteOrder order);

public:
    explicit Backend(QString name, Engine * engine, bool hasProgress = false);
    virtual void play() = 0;
    virtual void stop() = 0;
    bool reset() override;
    int sampleRate() const;
    SampleType sampleType() const;
    int sampleSize() const;
    ByteOrder byteOrder() const;
    qint32 bytesForDuration(qint64 usec) const;
    qreal volume() const;
    void setVolume(const qreal &volume);
    qreal buffer() const;
signals:
    void sampleRateChanged(int sampleRate);
    void sampleSizeChanged(int sampleSize);
    void sampleTypeChanged(SampleType sampleType);
    void byteOrderChanged(ByteOrder byteOrder);
    void bufferChanged(qreal buffer);

    // QIODevice interface
public:
    bool open(OpenMode mode) override;
    void close() override;
};

#endif // BACKEND_H
